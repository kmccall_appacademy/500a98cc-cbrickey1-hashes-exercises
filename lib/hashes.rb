# require "byebug"
# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  my_hash = Hash.new
  str.split(/\s+/).each { |word| my_hash[word] = word.length }
  my_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  values_arr = hash.values
  hash.each { |k, v| return k if v == values_arr.max }
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each { |k, v| older[k] = v }
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  word_hash = Hash.new(0)
  word.chars.each { |char| word_hash[char] += 1 }
  word_hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  my_hash = Hash.new(0)
  arr.each { |item| my_hash[item] += 1 }
  my_hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  even_odd_hash = Hash.new(0)
  numbers.each { |n| n % 2 == 0 ? even_odd_hash[:even] += 1 : even_odd_hash[:odd] += 1 }
  even_odd_hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = %w{ a e i o u }

  vowel_hash = Hash.new(0)
  string.chars.each { |char| vowel_hash[char] += 1 if vowels.include?(char) }

  most_common_count = vowel_hash.values.max
  sorted_char_array = vowel_hash.keys.sort

  sorted_char_array.each do |char|
    return char if vowel_hash[char] == most_common_count
  end

end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  late_birthdays_hash = students.select { |k, v| v > 6 }
  student_array = late_birthdays_hash.keys

  combo_array = Array.new
  student_array.each_with_index do |student1, index1|
    (index1 + 1).upto(student_array.length - 1) do |index2|
      student2 = student_array[index2]
      combo_array << [student1, student2]
    end
  end

  combo_array
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula:
# number_of_species**2 * smallest_population_size / largest_population_size

# biodiversity_index(["cat", "cat", "cat"])  => 1
# biodiversity_index(["cat", "leopard-spotted ferret","dog"]) => 9

def biodiversity_index(specimens)
  animal_hash = Hash.new(0)
  specimens.each { |animal| animal_hash[animal] += 1}

  number_of_species = animal_hash.size
  population_size_array = animal_hash.values

  (number_of_species**2) * population_size_array.min / population_size_array.max
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.

# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_hash = make_hash_counter(normal_sign)
  vandal_hash = make_hash_counter(vandalized_sign)
  vandal_hash.all? { |k, v| v <= normal_hash[k] }
end

def make_hash_counter(str)
  new_hash = Hash.new(0)
  str.gsub(/\s+/, "")
  str.chars.each { |char| new_hash[char.downcase] += 1 }
  new_hash
end

# def character_count(str)
# end
